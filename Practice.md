
Выполнение действий приведенных в методичке позволит познакомиться с такими утилитами , как ip, tcpdump, и tracepath. Получить базовые навыки работы со статической маршрутизацией. Получить навыки назначения маршрутов по умолчанию. Так же вы получите навыки по поиску проблем с маршрутизацией в сети небольшого размера.

# Практическая часть
Для выполнения ДЗ подготовлен стенд из трех ВМ:
- internetRouter, centos6. Выступает в роли приграничного роутера.
- centralRouter, centos7. 
- centralServer, centos7.

```
centralServer --> centralRouter -->inetRouter --> internet
```
Подсети:
-	centralRouter -->inetRouter: 192.168.255.0/30
-	centralServer --> centralRouter: 192.168.0.32/28

Утилиты используемые в ДЗ:
- ip (addr, route)
- sysctl

Утилиты для проверки работоспособности и поиска проблем:
- ping
- tcpdump
- tracepath

## Настройка сегмента сети inetRouter-centralRouter. Настройка доступа в Internet centralRouter через inetRouter

При создании ВМ Vagrant конфигурирует маршрут по умолчанию через интерфейсе eth0 (обычно с IP адресом из подсети 10.0.2.0/24)

Проверим маршрут по умолчанию:
```
[root@centralRouter vagrant]# ip route 
default via 10.0.2.2 dev eth0 proto dhcp metric 100 <==  маршрут по умолчанию
10.0.2.0/24 dev eth0 proto kernel scope link src 10.0.2.15 metric 100
192.168.0.32/28 dev eth2 proto kernel scope link src 192.168.0.33 metric 102
192.168.255.0/30 dev eth1 proto kernel scope link src 192.168.255.2 metric 101
 ```

Чтобы добиться хождения трафика внутри смоделированной сети, отключаем маршрут по умолчанию через eth0 на всех ВМ кроме internetRouter(internetRouter через eth0 будет предоставлять Interenet стенду)
```
ip route delete default
```
Проверяем, что маршрут по умолчанию удален:

```
[root@centralRouter vagrant]# ip route
10.0.2.0/24 dev eth0 proto kernel scope link src 10.0.2.15 metric 100
192.168.0.32/28 dev eth2 proto kernel scope link src 192.168.0.33 metric 102
192.168.255.0/30 dev eth1 proto kernel scope link src 192.168.255.2 metric 101
```

Добавим новый маршрут по умолчанию для centralRouter.

Для centralRouter шлюзом по умолчанию будет интерфейс inetRouter с IP 192.168.255.1.

```
ip route add default via 192.168.255.1
```
Проверим добавление маршрута:
```
ip route
default via 192.168.255.1 dev eth1  <==  видим, что маршрут по умолчанию добавлен
10.0.2.0/24 dev eth0 proto kernel scope link src 10.0.2.15 metric 100
192.168.0.32/28 dev eth2 proto kernel scope link src 192.168.0.33 metric 102
192.168.255.0/30 dev eth1 proto kernel scope link src 192.168.255.2 metric 101
```
Проверим доступность inetRouter с centralRouter.
```
[root@centralRouter vagrant]# ping 192.168.255.1 -c 1
PING 192.168.255.1 (192.168.255.1) 56(84) bytes of data.
64 bytes from 192.168.255.1: icmp_seq=1 ttl=64 time=0.788 ms
```
Видим что пинг успешен, т.к. сетевые интерфейсы серверов находятся в одной физической(эмулироанной) сети и имеют адресацию из одной подсети.

Попробуем пропинговать 8.8.8.8:
```
[root@centralRouter vagrant]# ping 8.8.8.8 -w 1ы
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.

--- 8.8.8.8 ping statistics ---
1 packets transmitted, 0 received, 100% packet loss, time 0ms

```
Видим, что потеря 100% пакетов.

Для траблшутинга запустим на centralRouter ``` ping 8.8.8.8 ```, а на inetRouter  ``` tcpdump -i eth1 icmp ``` чтобы посмотреть что происходит с пакетами icmp на inetRouter.

```
[root@inetRouter vagrant]# tcpdump -i eth1 icmp
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on eth1, link-type EN10MB (Ethernet), capture size 65535 bytes
21:22:16.183376 IP 192.168.255.2 > dns.google: ICMP echo request, id 8035, seq 1, length 64
```
Видим, что пакеты доходят, но нет reply.

Дело в том, что на inetRouter отключен форвардинг, и сервер не маршрутизирует трафик между сетевыми интерфейсами.
Проверим это.
```
[root@inetRouter vagrant]# cat /proc/sys/net/ipv4/ip_forward
0  <== "0" говорит о том форвардинг выключен
``` 

Исправим это, включив форвардинг на inetRouter
```
sysctl net.ipv4.conf.all.forwarding=1
```
Внимание!

Форвардинг стоит включить на всех ВМ стенда, выступающих в роли роутеров.

```
[root@inetRouter vagrant]# cat /proc/sys/net/ipv4/ip_forward
1 <== форвардинг включен
```
Заново пропингуем `` ping 8.8.8.8 ```

```
[root@centralRouter vagrant]# ping 8.8.8.8 -w 1
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=61 time=50.1 ms

--- 8.8.8.8 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 50.128/50.128/50.128/0.000 ms
```
Видим, что пинг прошел успешно.
Доступ centralRouter в Internet через inetRouter предоставлен.

## Настройка сегмента сети centralRouter-centralServer. Настройка доступа в Internet для centralServer через inetRouter через промежуточный роутер centralRouter.

Т.к centralRouter выступает в роли маршрутизатора, включим форвардинг:
```
sysctl net.ipv4.conf.all.forwarding=1
```

На centralServer удалим маршрут по умолчанию через eth0:
```
ip route delete default
```
Добавим новый маршрут по умолчанию. Для centralServer шлюзом по умолчанию будет выступать интерфейс с IP 192.168.0.33 на centralRouter:
```
ip route add default via 192.168.0.33
```

Вроде бы все сделали правильно.
Для начала попробуем пропинговать inetRouter:
```
[root@centralServer vagrant]# ping 192.168.255.1 -w 1
PING 192.168.255 (192.168.0.255) 56(84) bytes of data.

--- 192.168.255 ping statistics ---
1 packets transmitted, 0 received, 100% packet loss, time 0ms
```
Опять ``` 100% packet loss ``` =(

Проверим маршруты:
```
[root@centralServer vagrant]# ip route
default via 192.168.0.33 dev eth1
10.0.2.0/24 dev eth0 proto kernel scope link src 10.0.2.15 metric 100
192.168.0.32/28 dev eth1 proto kernel scope link src 192.168.0.34 metric 101
```
Маршрут по умолчанию прописан верно.

Снова воспользуемся утилитой tcpdump на inetRouter и параллельно запустим ping 192.168.255.1 на centralServer:
```
[root@inetRouter vagrant]# tcpdump -i eth1 icmp
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on eth1, link-type EN10MB (Ethernet), capture size 65535 bytes
21:50:57.505122 IP 192.168.0.34 > 192.168.255.1: ICMP echo request, id 24895, seq 1, length 64
21:50:58.504853 IP 192.168.0.34 > 192.168.255.1: ICMP echo request, id 24895, seq 2, length 64
21:50:59.510438 IP 192.168.0.34 > 192.168.255.1: ICMP echo request, id 24895, seq 3, length 64
```

Видим только request,а reply отсутсвует. inetRouter не отправляет reply. 
Может inetRouter не знает куда отправлять?

Посмотрим маршруты на inetRouter:
```
[root@inetRouter vagrant]# ip route
192.168.255.0/30 dev eth1  proto kernel  scope link  src 192.168.255.1
10.0.2.0/24 dev eth0  proto kernel  scope link  src 10.0.2.15
169.254.0.0/16 dev eth0  scope link  metric 1002
169.254.0.0/16 dev eth1  scope link  metric 1003
default via 10.0.2.2 dev eth0
```
Видим, что inetRouter ничего не знает о подсети ``` 192.168.0.32/28 ```

Добавим новый маршрут:
```
ip route add 192.168.0.32/28 via 192.168.255.2 dev eth1
```
Проверим маршруты:
```
[root@inetRouter vagrant]# ip route
192.168.255.0/30 dev eth1  proto kernel  scope link  src 192.168.255.1
192.168.0.32/28 via 192.168.255.2 dev eth1                                 <== маршрут добавился
10.0.2.0/24 dev eth0  proto kernel  scope link  src 10.0.2.15
169.254.0.0/16 dev eth0  scope link  metric 1002
169.254.0.0/16 dev eth1  scope link  metric 1003
default via 10.0.2.2 dev eth0
```
Запустим еще раз ``` ping 192.168.255.1 ```:
```
[root@centralServer vagrant]# ping 192.168.255.1 -w 1
PING 192.168.255.1 (192.168.255.1) 56(84) bytes of data.
64 bytes from 192.168.255.1: icmp_seq=1 ttl=63 time=0.695 ms

--- 192.168.255.1 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.695/0.695/0.695/0.000 ms
```
И  ```   ping 8.8.8.8 ```:
```
[root@centralServer vagrant]# ping 8.8.8.8 -w 1
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=59 time=91.4 ms

--- 8.8.8.8 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 91.445/91.445/91.445/0.000 ms
```
Пинги прошли уcпешно!

На всякий случай сделаем "обратный" пинг centralSevrer c inetRouter:
```
[root@inetRouter vagrant]# ping 192.168.0.34 -c 1
PING 192.168.0.34 (192.168.0.34) 56(84) bytes of data.
64 bytes from 192.168.0.34: icmp_seq=1 ttl=63 time=1.18 ms

--- 192.168.0.34 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 1ms
rtt min/avg/max/mdev = 1.187/1.187/1.187/0.000 ms
```
Запустим утилиту tracepath для проверки через какие узлы уходит трафик:
```
[root@centralServer vagrant]# tracepath -n 8.8.8.8
 1?: [LOCALHOST]                                         pmtu 1500
 1:  192.168.0.33                                          2.640ms
 1:  192.168.0.33                                          2.646ms
 2:  192.168.255.1                                         2.737ms
 ```
Видим, что в списке присутствуют два нами настроенных роутера.

В итоге проделанной работы получили стенд из трех узлов с доступом в Internet.
Научились настраивать статическую маршрутизацию. Включать форвардинг.
Попробовали элементы траблшутинга.
В проделанной работе настроили роутинг и включили форвардинг не перманентно. Настройки роутинга и форвардинга сбросятся при перезагрузке стенда.
Настройка перманентного назначения маршрута отводится на самостоятельную работу и относится к ДЗ с (*).

## Самостоятельная работа

Добавить в стенд 4 ВМ:
- office1Server
- office2Server
- office1Router
- office2Router

ВМ добавляются в стенд путем редактирования Vagrantfile.
На centralRouter потребуется добавление еще несколько стетевых интерфейсов.
Сетевые интерфейсы ВМ "смотрящие" друг на друга должны иметь одинаковый ``` virtualbox__intnet ```.

Соеденить их согласно схеме:
```
office1Server --> office1Router  --\
                                    \
             centralServer --> centralRouter -->inetRouter --> internet
                                    /
office2Server --> office2Router  --/
```

Использовать следующие подсети:
-	centralRouter -->inetRouter: 192.168.255.0/30
-	centralServer --> centralRouter: 192.168.0.32/28 
-	centralRouter--> office1Router, office2Router: выбрать подсети самостоятельно достаточные  для сетевого соединения двух роутеров. Выбрать  из свободного диапазона подсети 192.168.255.0/24.
-	office1Server --> office1Router: 192.168.1.192/26
-	office2Server --> office2Router: 192.168.2.192/26

## Критерии оценки.
- 5 баллов. Выполнена теоретическая часть. Собран стенд из семи ВМ, все ВМ видят друг друга. Доступ ВМ в интернет через inetRouter.
- 6 баллов. Выполнена теоретическая часть. Собран стенд из семи ВМ, все ВМ видят друг друга. Доступ ВМ в интернет через inetRouter. Стенд сохраняет работоспособность после перезагрузки.
- 7 баллов. При выполнении ДЗ использовался агрегированный маршрут.

Выполнения ранее описанных действий, выполнение всех пунктов самостоятельной работы и теоретической части являются необходимым для получения зачета по базовому домашнему заданию. Приветствуется составление карты сети. Для проверки вам будет необходимо прислать ссылку на ваш репозиторий в чат с преподавателем в Личном кабинете. Репозиторий, соответственно, должен быть публичным. На все возникшие вопросы можно получить ответ в переписке в чате с преподавателем или, что более рекомендуется, в слаке вашей группы.