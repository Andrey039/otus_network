# otus-linux
Vagrantfile - для стенда урока 9 - Network

# Дано
https://gitlab.com/Andrey039/otus_network/-/blob/master/Vagrantfile

Vagrantfile с начальным  построением сети

```
centralServer --> centralRouter -->inetRouter --> internet

```


тестировалось на virtualbox

# Теоретическая часть
- Найти свободные подсети
- Посчитать сколько узлов в каждой подсети, включая свободные
- Указать broadcast адрес для каждой подсети
- проверить нет ли ошибок при разбиении

Сеть office1
- 192.168.2.0/26      - dev
- 192.168.2.64/26    - test servers
- 192.168.2.128/26  - managers
- 192.168.2.192/26  - office hardware

Сеть office2
- 192.168.1.0/25      - dev
- 192.168.1.128/26  - test servers
- 192.168.1.192/26  - office hardware


Сеть central
- 192.168.0.0/28    - directors
- 192.168.0.32/28  - office hardware
- 192.168.0.64/26  - wifi

# Практическая часть
- Соединить офисы в сеть согласно схеме и настроить роутинг
- Все сервера и роутеры должны ходить в инет черз inetRouter
- Все сервера должны видеть друг друга
- у всех новых серверов отключить дефолт на нат (eth0), который Vagrant поднимает для связи
- при нехватке сетевых интервейсов добавить по несколько адресов на интерфейс


```
office1Server --> office1Router  --\
                                    \
             centralServer --> centralRouter -->inetRouter --> internet
                                    /
office2Server --> office2Router  --/
```

Используемые подсети:
-	centralRouter -->inetRouter: 192.168.255.0/30
-	centralServer --> centralRouter: 192.168.0.32/28 
-	centralRouter--> office1Router, office2Router: выбрать подсети самостоятельно достаточные  для сетевого соединения двух роутеров. Выбрать  из свободного диапазона подсети 192.168.255.0/24.
-	office1Server --> office1Router: 192.168.1.192/26
-	office2Server --> office2Router: 192.168.2.192/26
